INSTALLED_APPS = (
    'django_journal',
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sessions',
)
DATABASES = {
    'default': {'ENGINE': 'django.db.backends.postgresql_psycopg2', 'NAME': '_test'},
    'error': {'ENGINE': 'django.db.backends.postgresql_psycopg2', 'NAME': '_test'},
}

SECRET_KEY = 'django_tests_secret_key'
PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher',)
ROOT_URLCONF = 'test_urls'

JOURNAL_DB_FOR_ERROR_ALIAS = 'error'
